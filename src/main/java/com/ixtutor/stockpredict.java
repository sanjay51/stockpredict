package com.ixtutor;

import com.ixtutor.predictor.PredictorNet;
import com.ixtutor.predictor.Stock;
import com.ixtutor.predictor.StockData;

public class stockpredict {
    static int nIn = 1;
    static int nOut = 3;

    public static void main(String[] args) throws Exception {
        PredictorNet predictorNet = new PredictorNet(nIn, nOut);
        StockData stockData = new StockData(Stock.AMZN_test);

        predictorNet.train(stockData);
    }
}
