package com.ixtutor.utils;

import java.io.IOException;

public class Assert {
    public static void assertTrue(boolean truthStatement) {
        if (truthStatement != true) throw new IllegalArgumentException();
    }
    public static void assertExists(boolean existanceTruthStatement) throws Exception {
        if (existanceTruthStatement != true) throw new IOException("File could not be downloaded or doesn't exist.");
    }
}
