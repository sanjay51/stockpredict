package com.ixtutor;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;

public class UI {
    UIServer uiServer;
    StatsStorage statsStorage;

    public UI() {
        this.uiServer = UIServer.getInstance();
        statsStorage = new InMemoryStatsStorage();
        uiServer.attach(statsStorage);
    }

    public StatsStorage getStatsStorage() {
        return this.statsStorage;
    }
}
