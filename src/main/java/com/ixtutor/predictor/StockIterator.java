package com.ixtutor.predictor;

import org.datavec.api.records.reader.SequenceRecordReader;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;

public class StockIterator extends SequenceRecordReaderDataSetIterator {
    DataNormalization normalizer;

    public StockIterator(SequenceRecordReader featuresReader, int batchSize, int miniBatchSize, int numPossibleLabels) {
        super(featuresReader, batchSize, miniBatchSize, numPossibleLabels);

        this.normalizer = new NormalizerMinMaxScaler();
        normalizer.fit(this);
    }

    @Override
    public DataSet next() {
        return super.next();
    }

    public DataSet nextNormalized() {
        DataSet dataset = this.next();
        normalizer.transform(dataset);
        return dataset;
    }
}
