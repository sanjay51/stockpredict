package com.ixtutor.predictor;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static org.nd4j.linalg.ops.transforms.Transforms.abs;
import static org.nd4j.linalg.ops.transforms.Transforms.floor;

public class PredictorNet {
    private static Logger log = Logger.getLogger("predictornetlogger");

    MultiLayerNetwork neuralNet;

    static int lstmLayerSize = 20;
    static int tBPTTLength = 30;

    static int epochs = 1;

    public PredictorNet(int nIn, int nOut) throws Exception {
        MultiLayerConfiguration configuration = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .learningRate(0.01)
                .rmsDecay(0.95)
                .seed(12345)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.RMSPROP)
                .list()
                .layer(0, new GravesLSTM.Builder()
                        .nIn(nIn)
                        .nOut(lstmLayerSize)
                        .activation(Activation.TANH)
                        .build())
                .layer(1, new RnnOutputLayer.Builder()
                        .nIn(lstmLayerSize)
                        .nOut(nOut)
                        .activation(Activation.SOFTMAX)
                        .build())
                .backpropType(BackpropType.TruncatedBPTT)
                .tBPTTForwardLength(tBPTTLength)
                .tBPTTBackwardLength(tBPTTLength)
                .build();

        MultiLayerNetwork neuralNet = new MultiLayerNetwork(configuration);
        neuralNet.init();
        neuralNet.setListeners(new ScoreIterationListener(1));
                //new StatsListener(new UI().getStatsStorage()));

        this.neuralNet = neuralNet;
    }

    public MultiLayerNetwork getNeuralNet() {
        return this.neuralNet;
    }

    public void train(StockData stockData) {
        StockIterator iterator = stockData.getTrainingDataSetIterator();
        iterator.reset();

        IntStream.range(0, epochs)
                .boxed()
                .forEach(i -> {
                    iterator.reset();
                    int count = 0;
                    while (iterator.hasNext() && count++ < 1000) {
                        this.load();
                        DataSet dataSet = iterator.nextNormalized();
                        this.neuralNet.fit(dataSet);

                        this.save();
                        test(stockData);
                        iterator.reset();
                        count++;
                    }

                    log.info("Epoch " + i + ": Iteration complete.");
                });
    }

    public void save() {
        try {
            File locationToSave = new File("trainednet.zip");
            ModelSerializer.writeModel(this.getNeuralNet(), locationToSave, true);
        } catch (Exception e) {
            System.out.println("Caught exception while saving net: " + e);
        }
    }

    public void load() {
        try {
            File locationToSave = new File("trainednet.zip");
            this.neuralNet = ModelSerializer.restoreMultiLayerNetwork(locationToSave);
        } catch (Exception e) {
            System.out.println("Caught exception while RESTORING net: " + e);
        }
    }

    public void test(StockData stockData) {
        System.out.println("Testing start.");
        StockIterator evalIterator = stockData.getEvalDataSetIterator();

        evalIterator.reset();
        Path filePath = Paths.get("output.txt");
        //this.neuralNet.rnnClearPreviousState();

        while (evalIterator.hasNext()) {
            DataSet dataSet = evalIterator.nextNormalized();

            INDArray output = this.neuralNet.rnnTimeStep(dataSet.getFeatures());
            publishResults(dataSet, output, filePath);
        }

        System.out.println("Testing done.");
    }

    private void publishResults(DataSet input, INDArray output, Path filePath) {
        INDArray ideal1stRow = input.getLabels().getRow(0).getRow(0);
        INDArray output1stRow = output.getRow(0).getRow(0);

        INDArray transformed = floor(abs(ideal1stRow.sub(output1stRow)).add(0.50));
        double count = ideal1stRow.size(1);
        double accuracy = (count - transformed.sumNumber().doubleValue()) * (100.0/count);

        List<String> lines = new ArrayList<>();
        lines.add("IDEAL    : " + ideal1stRow);
        lines.add("PREDICTED:" + output1stRow);
        //lines.add("Subtr :" + inp1stRow.sub(output1stRow));
        //lines.add("Abs   :" + abs(inp1stRow.sub(output1stRow)));
        //lines.add("Add0.5:" + abs(inp1stRow.sub(output1stRow)).add(0.5));
        //lines.add("Floor :" + floor(abs(inp1stRow.sub(output1stRow)).add(0.5)));
        //lines.add("IDEAL: " + input.getLabels());
        lines.add("Accuracy: " + accuracy + "%");

        lines.stream().forEach( line -> System.out.println(line));
        //writeToFile(lines, filePath);
    }

    private void writeToFile(List<String> lines, Path filePath) {
        try {
            Files.write(filePath, lines);
        } catch (IOException e) {
            System.out.println("Failed to write test output to file: " + e);
        }
    }
}
