package com.ixtutor.predictor;

import org.datavec.api.util.ClassPathResource;

import java.io.File;
import java.io.IOException;

public enum Stock {
    AMZN("amzn_1min_26Jan17_17Jun17.csv"),
    AMZN_test("amzn_sample_full_1.csv");

    String fileName;
    Stock(String fileName) {
        this.fileName = fileName;

    }

    public File getRawDataFile() throws IOException {
        return new ClassPathResource("/stock/" + this.fileName).getFile();
    }

    public File getDataFile() throws IOException {
        String tempDir = System.getProperty("java.io.tmpDir");
        return new File(tempDir + "/" + this.fileName);
    }

    public File getDataFileIndexed(int index) throws IOException {
        String tempDir = System.getProperty("java.io.tmpDir");
        return new File(tempDir + "/" + index + "_" + this.fileName);
    }

    public File getEvalFile() throws IOException {
        String tempDir = System.getProperty("java.io.tmpDir");
        return new File(tempDir + "/" + this.fileName.replace(".csv", ".csv.eval"));
    }
}
