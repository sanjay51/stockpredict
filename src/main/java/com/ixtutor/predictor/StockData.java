package com.ixtutor.predictor;

import org.apache.commons.lang.StringUtils;
import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.csv.CSVSequenceRecordReader;
import org.datavec.api.split.NumberedFileInputSplit;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class StockData {
    private static Logger log = Logger.getLogger("predictornetlogger");

    Stock stock;
    int batchSize = 1;
    StockIterator trainingIterator;
    StockIterator evalIterator;
    List<List<String>> processedLines;

    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");
    static Date TRADING_START_TIME;
    static Date TRADING_END_TIME;
    static Date TRADING_END_TIME_5MIN_BEFORE;

    static final String COMMA_SEPARATOR = ",";

    public StockData(Stock stock) throws Exception {
        this.stock = stock;

        TRADING_START_TIME = DATE_FORMAT.parse("06:30:00.000");
        TRADING_END_TIME = DATE_FORMAT.parse("13:01:00.000");
        TRADING_END_TIME_5MIN_BEFORE = DATE_FORMAT.parse("12:55:00.000");

        // preprocess();
        process();
    }

    private void process() throws Exception {
        SequenceRecordReader readerTrain = new CSVSequenceRecordReader(0, ",");
        System.out.println(stock.getDataFile().getAbsolutePath().replaceFirst("1", "%d"));
        readerTrain.initialize(new NumberedFileInputSplit(stock.getDataFile().getAbsolutePath().replaceFirst("1", "%d"), 1, 1));
        trainingIterator = new StockIterator(readerTrain, batchSize, 3, 0);

        SequenceRecordReader readerEval = new CSVSequenceRecordReader(0, ",");
        readerEval.initialize(new NumberedFileInputSplit(stock.getEvalFile().getAbsolutePath().replaceFirst("1", "%d"), 1, 1));
        evalIterator = new StockIterator(readerEval, batchSize, 3, 0);
    }

    public StockIterator getTrainingDataSetIterator() {
        return this.trainingIterator;
    }

    public StockIterator getEvalDataSetIterator() {
        return this.evalIterator;
    }

    private void preprocess() throws Exception {
        // transform raw file downloaded from internet to one suitable for training and validation
        File rawFile = this.stock.getRawDataFile();
        List<String> lines = Files.readAllLines(rawFile.toPath());
        processedLines = transformLines(lines);

        processedLines.stream().forEach(day -> {
            if (day.size() != getTradingMinutesInDay()) {
                log.severe("Illegal day found with minutes: " + day.size());
                System.out.println("== Illegal day start ==");
                day.stream().forEach(line -> System.out.println(line));
                System.out.println("== Illegal day end ==");
            }
        });

        for (int i = 0; i < processedLines.size(); i++) {
            List<String> day = processedLines.get(i);
            Files.write(stock.getDataFile().toPath(), day);
        }

        log.info("Saved processed lines in file: " + stock.getDataFile());
        log.info("Raw data processing complete.");
    }

    private List<List<String>> transformLines(List<String> lines) throws Exception {
        List<List<String>> dayData = new ArrayList<>();

        List<String> currentDay = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);

            if (StringUtils.isBlank(line)) continue;
            if (line.startsWith("Local")) continue;

            /* Sample data format
             * Local time,Open,High,Low,Close,Volume
             * 26.01.2017 00:00:00.000,836.501,836.501,836.501,836.501,0
             */

            TimePoint timePoint = new TimePoint(line);

            if (isEndOfDay(timePoint.date)) {
                dayData.add(currentDay);
                currentDay = new ArrayList<>();
                count = 0;
            }

            if (!isWithinTradingWindow(timePoint.date)) continue;
            if (isWithinLast5(timePoint.date)) continue; // discard last 5 minutes of data

            currentDay.add(timePoint.getProcessedLine(count, new TimePoint(lines.get(i+5))));
            count++;
        }

        return dayData;
    }

    class TimePoint {
        String date;
        String open;
        String high;
        String low;
        String close;

        public TimePoint(String line) {
            String[] parts = line.split(COMMA_SEPARATOR);
            date = parts[0];
            open = parts[1];
            high = parts[2];
            low = parts[3];
            close = parts[4];
        }

        public String getProcessedLine(int index, TimePoint timePoint5minLater) {
            return getRelativeScore(timePoint5minLater) + "," + close;
        }

        private int getRelativeScore(TimePoint timePoint5minLater) {
            double closeNow = Double.valueOf(close);
            double close5minLater = Double.valueOf(timePoint5minLater.close);

            if (close5minLater < closeNow) return 0; //loss
            if (close5minLater == closeNow) return 1; //no loss no gain
            if (close5minLater > closeNow) return 2; //gain

            return 4; // what the hell?
        }
    }


    private boolean isWithinTradingWindow(String date) throws Exception {
        //e.g. 26.01.2017 00:00:00.000

        String[] parts = date.split(" ");
        String timeStr = parts[1];
        Date time = DATE_FORMAT.parse(timeStr);

        return time.after(TRADING_START_TIME) && time.before(TRADING_END_TIME);
    }

    private boolean isWithinLast5(String date) throws Exception {
        //e.g. 26.01.2017 00:00:00.000

        String[] parts = date.split(" ");
        String timeStr = parts[1];
        Date time = DATE_FORMAT.parse(timeStr);

        return time.after(DATE_FORMAT.parse("12:55:00.000"));

    }

    private boolean isEndOfDay(String date) throws Exception {
        return "23:59:00.000".equals(date.split(" ")[1]); //e.g. "26.01.2017 23:59:00.000"
    }

    private int getTradingMinutesInDay() {
        int windowNotConsidered = 5; // 5 min. at end of each day is disregarded
        final int TRADING_MINUTES_IN_DAY = 390 - windowNotConsidered; //6*60 + 30 (6hours30min)
        return TRADING_MINUTES_IN_DAY;
    }
}
